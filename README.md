# SSHTUI

## Description
`sshtui` is a cli tool based on `ssh` for managing ssh connecctions via terminal. 
Inspired by `lf` and `ranger` file managers, written in python curses. 

It is lightweight (~250 lines of code) and easy to customize.
Supports mouse navigation as well as keyboard navigation.

## Key bindings
### For ssh
You can customize these bindings or add extra bindings in `KEY_BINDINGS` variable of `sshtui` in a row #6:
- `Enter` - connect to host

### Other:
- letters - enter filter mode and apply filter
- `Escape` - exit filter mode or `sshtui` itself
- `Backspace` - remove letter from filter
- arrow keys, `PgUp`, `PgDn`, `Home`, `End` - navigation


## Dependencies
- `python3`
- `~/.ssh/config` file created with hosts inside it

## Installation
Download latest `sshtui`:
```
curl -O "https://git.digitalstudium.com/digitalstudium/sshtui/raw/branch/main/sshtui"
```
Then install it:
```
sudo install ./sshtui /usr/local/bin/ && rm -f ./sshtui
```

